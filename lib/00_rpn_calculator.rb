class RPNCalculator
  def initialize
    @stack = []
  end

  def push(num)
    @stack << num
  end

  def value
    @stack.last
  end

  def plus
    perform_operation(:+)
  end

  def minus
    perform_operation(:-)
  end

  def times
    perform_operation(:*)
  end

  def divide
    perform_operation(:/)
  end

  def tokens(string)
    tokens = string.split
    tokens.map do |token|
      symbol?(token) ? token.to_sym : token.to_i
    end
  end

  def evaluate(string)
    tokens = tokens(string)
    tokens.each do |token|
      case token
      when Integer
        push(token)
      else
        perform_operation(token)
      end
    end

    value
  end

  private

  def symbol?(token)
    [:+, :-, :*, :/].include?(token.to_sym)
  end

  def perform_operation(op_symbol)
    raise "calculator is empty" if @stack.length < 2

    second_number = @stack.pop
    first_number = @stack.pop

    case op_symbol
    when :+
      @stack << first_number + second_number
    when :-
      @stack << first_number - second_number
    when :*
      @stack << first_number * second_number
    when :/
      @stack << first_number.fdiv(second_number)
    else
      @stack << first_number
      @stack << second_number
      raise "There is no such operation for #{op_symbol}"
    end
  end
end
